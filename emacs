(menu-bar-mode 1)

(add-to-list 'load-path "~/.emacs.d/color-theme")

(require 'color-theme)
(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     (color-theme-sunburst)))

(custom-set-variables
;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(global-linum-mode t)
 '(global-visual-line-mode t)
 '(inhibit-startup-screen t)
 '(initial-buffer-choice t)
 '(mouse-wheel-mode t)
 '(mouse-wheel-progressive-speed nil)
 '(mouse-wheel-scroll-amount (quote (2 ((shift) . 1) ((control)))))
 '(show-paren-mode t)
 '(standard-indent 0)
 '(truncate-lines t))
;; (custom-set-faces
;;   ;; custom-set-faces was added by Custom.
;;   ;; If you edit it by hand, you could mess it up, so be careful.
;;   ;; Your init file should contain only one such instance.
;;   ;; If there is more than one, they won't work right.
;;  )

;; (add-to-list 'load-path "~/.emacs.d/slime-2010-10-19/contrib")
;; (add-hook 'slime-load-hook (lambda () (require 'slime-scheme)))

;; (setq inferior-lisp-program "/usr/local/bin/racket")

;(add-to-list 'load-path "~/.emacs.d/textmate.el")
;(require 'textmate)
;(textmate-mode)

;; (add-hook 'lisp-mode-hook '(lambda ()
;;                              (local-set-key (kbd "RET") 'newline-and-indent)))

;; Put autosave files (ie #foo#) in one place, *not*
;; scattered all over the file system!
(defvar autosave-dir
  (concat "/tmp/emacs_autosaves/" (user-login-name) "/"))

(make-directory autosave-dir t)

(defun auto-save-file-name-p (filename)
  (string-match "^#.*#$" (file-name-nondirectory filename)))

(defun make-auto-save-file-name ()
  (concat autosave-dir
	  (if buffer-file-name
	      (concat "#" (file-name-nondirectory buffer-file-name) "#")
	    (expand-file-name
	     (concat "#%" (buffer-name) "#")))))

;; Put backup files (ie foo~) in one place too. (The backup-directory-alist
;; list contains regexp=>directory mappings; filenames matching a regexp are
;; backed up in the corresponding directory. Emacs will mkdir it if necessary.)
(defvar backup-dir (concat "/tmp/emacs_backups/" (user-login-name) "/"))
(setq backup-directory-alist (list (cons "." backup-dir)))

(fset 'insertPound "#")
(global-set-key (kbd "M-3") 'insertPound)

(setq-default indent-tabs-mode nil)
(setq-default tab-width 2)
(setq indent-line-function 'insert-tab)

(setq c-default-style "linux"
      c-basic-offset 2)

(c-add-style "my-c-style" '((c-continued-statement-offset 0))) ; If a statement continues on the next line, indent the continuation by 4
(defun my-c-mode-hook ()
  (c-set-style "my-c-style")
  (c-set-offset 'substatement-open '0) ; brackets should be at same indentation level as the statements they open
  (c-set-offset 'topmost-intro '/)
  (c-set-offset 'inline-open '/)
  (c-set-offset 'block-open '/)
  (c-set-offset 'brace-list-open '/)   ; all "opens" should be indented by the c-indent-level
  (c-set-offset 'case-label '0)
  (c-set-offset 'defun-block-intro '*))       ; indent case labels by c-indent-level, too
(add-hook 'c-mode-hook 'my-c-mode-hook)
(add-hook 'c++-mode-hook 'my-c-mode-hook)
(add-hook 'csharp-mode-hook 'my-c-mode-hook)
(global-set-key (kbd "M-ESC") 'hippie-expand)
(global-set-key (kbd "s-{") 'previous-buffer)
(global-set-key (kbd "s-}") 'next-buffer)


;; (autoload 'go-mode "~/.emacs.d/go-mode.el" nil t)
;; (add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode))

;; (autoload 'glsl-mode "~/.emacs.d/glsl-mode" nil t)
;; (add-to-list 'auto-mode-alist '("\\.vert\\'" . glsl-mode))
;; (add-to-list 'auto-mode-alist '("\\.frag\\'" . glsl-mode))

;; (autoload 'markdown-mode "~/.emacs.d/markdown-mode/markdown-mode.el"
;;    "Major mode for editing Markdown files" t)
;; (setq auto-mode-alist
;;    (cons '("\\.md" . markdown-mode) auto-mode-alist))

;; (setq-default truncate-lines t)
;; (setq truncate-partial-width-windows nil) ;; for vertically-split windows

;; (autoload 'xcode "~/.emacs.d/emacs-xcode" nil t)

;; (setq auto-mode-alist
;;       (cons '("\\.m$" . objc-mode) auto-mode-alist))
;; (setq auto-mode-alist
;;       (cons '("\\.mm$" . objc-mode) auto-mode-alist))

(autoload 'csharp-mode "~/.emacs.d/csharp-mode.el" nil t)
(setq auto-mode-alist
      (cons '("\\.cs$" . csharp-mode) auto-mode-alist))


;;; This was installed by package-install.el.
;;; This provides support for the package system and
;;; interfacing with ELPA, the package archive.
;;; Move this code earlier if you want to reference
;;; packages in your .emacs.
(when
    (load
     (expand-file-name "~/.emacs.d/elpa/package.el"))
  (package-initialize))

if [ -f ~/.git-completion ]; then
  source ~/.git-completion
fi

if [ -f ~/.bash_aliases ]; then
  source ~/.bash_aliases
fi

if [ -f ~/.resty/resty ]; then
  source ~/.resty/resty
fi

export GEM_EDITOR='mate'

# colour terminal and add git branch name
#export CLICOLOR=1
#export LS_COLORS=exfxcxdxbxegedabagacad
export EDITOR="open -W -a Emacs.app"

export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/1.6/Home
export HADOOP_HOME=/Users/$USER/src/hadoop

# add mysql & mongo to path
export PATH="/usr/local/bin:/usr/local/sbin:/usr/local/mongodb/bin:/usr/local/mysql/bin:/usr/local/bin:/usr/local/Cellar/python/2.7/bin:$PATH"

_safe_branch_ps1()
{
  if [[ $(__git_ps1 "(%s)") =~ "(master)" ]]; then 
    __git_ps1 "(\033[0;32m%s\033[0m)" 
  else
    __git_ps1 "(\033[0;31m%s\033[0m)" 
  fi  
}
export PS1='\h:\W$(_safe_branch_ps1) \u\$ '
export NODE_PATH="/usr/local/lib/node"

export GOROOT=`brew --cellar go`/HEAD
export GOBIN=/usr/local/bin
export GOARCH=amd64
export GOOS=darwin


function authme { 
ssh $1 'cat >>~/.ssh/authorized_keys' <~/.ssh/id_dsa.pub 
}

alias em="open -a Emacs.app"

_mategem()
{
    local curw
    COMPREPLY=()
    curw=${COMP_WORDS[COMP_CWORD]}
    local gems="$(gem environment gemdir)/gems"
    COMPREPLY=($(compgen -W '$(ls $gems)' -- $curw));
    return 0
}
complete -F _mategem -o dirnames mategem

# PS3 Stuff

export PS3DEV=$HOME/Development/ps3
export PATH=$PATH:$PS3DEV/bin:$PS3DEV/ppu/bin:$PS3DEV/spu/bin
export PSL1GHT=$PS3DEV/PSL1GHT/psl1ght/build

# RVM


if [[ -s /Users/nk/.rvm/scripts/rvm ]] ; then source /Users/nk/.rvm/scripts/rvm ; fi

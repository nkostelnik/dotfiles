# git stuff
alias m="mate"
alias gst="git status --short -b"
alias gl="git pull"
alias gp="git push"
alias gd="git diff | mate"
alias gc="git commit -v"
alias gca="git commit -v -a"
alias gb="git branch"
alias gba="git branch -a"
alias gcl="git clean -f"

alias ll="ls -alc"
